#pragma once
#include "stdafx.h"
#include <string>
#include <iostream>
#include<windows.h>

using namespace std;


void gotoxy(int x,int y);

struct card
{	
	//friend class game;
	card();
	enum rodzaje{ pik, kier, karo, trefl };
	rodzaje kolor;
	int value;
	int state;
	int color;
	int visible;
	card * prev;
	card * next;
	void display();
};

class deck
{
public:
	int x,y;
	//card * HEAD, *TAIL;
	unsigned counter;


	card * HEAD, *TAIL;
	//konstruktor
	
	deck();
	~deck();
	card * erase(card *p);
	card * push_front(card *p);
	card * push_back(card * p);
	card * pop_front();
	card * pop_back();
	card * insert(card *p, card *behind);  // wstawianie p za behind
	unsigned size();
	card* index(unsigned n); // numerowanie kart od 1 nie od 0
	void display();
	void displayDown();
	void displayUp();
	void genDeck();
	card * pick(card * p);
	void shuffle();
	friend void gotoxy(int x,int y);
	void setxy(int x,int y);
	void setvisible();
	void gwiazdka();
	void delateColor(int ile_kart);//
	void setColor(int ile_kart);//w karcie
	friend void color(int c);// zmienia kolor

};

//istrukcja do color
//0 = Black
//1 = Blue
//2 = Green
//3 = Aqua
//4 = Red
//5 = Purple
//6 = Yellow
//7 = White
//8 = Gray
//9 = Light Blue
